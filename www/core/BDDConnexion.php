<?php

namespace App\Core;

interface BDDInterface
{
    public function connexion(
        string $dbDriver,
        string $dbAddress,
        string $dbName,
        string $dbUser,
        string $dbPass
    );

    public function query(string $query, array $parameters = [], string $fetchStyle = 'assoc', array $fetchOptions = []);
}
