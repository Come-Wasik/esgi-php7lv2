<!DOCTYPE html>
<html>
    <head>
        <title>Error</title>
        <meta charset="utf-8">
        <style>
            body {
                margin: 0px;
                background-image: linear-gradient(to right, Crimson, FireBrick);
                color: LightYellow;
            }
            h1, h2 {
                color: Wheat;
                font-size: 38px; margin: 0.2em 0px 0.5em 0px;
                text-shadow: 0px 0px 2px Cornsilk;
            }
            h2 { font-size: 30px;  margin: 0px; margin-left: 1em; }
            p { margin-left: 2em; }
            section#showErrorMessage {
                display: block;
                margin: 4em 0px 0px 3em;
                font-size: 23px;
                padding-top: 1em;
                padding-bottom: 2em;
            }
        </style>
    </head>
    <body>
        <main>
            <section id="showErrorMessage">
                <div class="block-to-center">
                    <h1>Oops, an error has occured</h1>
                    <h2>Error <?= $error->getCode(); ?></h2>
                    <p>Something seems wrong. Please contact us by mail if the problem persist.</p>
                </div>
            </section>
        </main>
    </body>
</html>