<!DOCTYPE html>
<html>
    <head>
        <title>Error</title>
        <meta charset="utf-8">
        <style>
            body {
                margin: 0px;
                background-color: Brown;
                color: LightYellow;
            }
            h1, h2 {
                color: Wheat;
                text-align: center;
            }
            h1 { font-size: 38px; margin: 0.2em 0px 0.5em 0px;}
            h2 { font-size: 32px;  margin: 0px; }
            section#showFirstError {
                background-image: linear-gradient(FireBrick 5%, Crimson);
                font-size: 23px;
                padding-top: 1em;
                padding-bottom: 2em;
            }
            section#listAllErrors {
                color : Crimson;
                font-size: 20px;
            }
            section#originalText {
                background-image: linear-gradient(FireBrick 5%, Crimson);
                font-size: 20px;
            }
            section#listAllErrors pre { font-size: 15px;}
            section#listAllErrors article {
                padding: 1em;
                box-shadow: 0px 0px 2px gray;
            }
            section#listAllErrors article:nth-child(odd) {  background-color: BlanchedAlmond; }
            section#listAllErrors article:nth-child(even) { background-color: Cornsilk; }
            section#originalText article { background-color: Brown; }
            .file, .line, .args, .reason {
                color: DarkRed;
                font-weight: bold;
            }
            section#listAllErrors .file, section#listAllErrors .line, section#listAllErrors .args, section#listAllErrors .reason {
                color: Crimson;
                font-weight: bold;
            }
            .file { font-size: 110%; }
            .line { font-size: 105%; }
            .args { font-size: 103%; }
            .block-to-center {
                display: block;
                width: 950px;
                margin: 0px auto 0px auto;
                box-sizing: border-box;
                padding: 1em;
            }
            .title-bar {
                box-shadow: -2px 0px 2px Crimson;
                background-image: linear-gradient(FireBrick 83%, Crimson );   padding: 1em;
            }
        </style>
    </head>
    <body>
        <main>
            <section id="showFirstError">
                <div class="block-to-center">
                    <h1>An error <?= $error->getCode(); ?> have occured</h1>
                    <p><span class="file">In file </span>: <?= $error->getFile(); ?><br />
                    At <span class="line">line <?= $error->getLine()?> </span> : <?= $error->getmessage(); ?></p>
                </div>
            </section>

            <section id="listAllErrors">
                <div class="title-bar">
                    <h2>Errors listing :</h2>
                </div>
                <?php foreach ($error->getTrace() as $error) { ?>
                    <article>
                        <div class="block-to-center">
                            <span class="file">In file </span>: <?= $error['file'] ?><br />
                            At <span class="line">line <?= $error['line'] ?> </span> :
                            <?php if (isset($error['function'])) { ?>
                                <?php if (isset($error['class'])) { ?>
                                    <?= $error['class']; ?>
                                <?php  } ?>

                                <?php if (isset($error['type'])) { ?>
                                    <?= $error['type']; ?>
                                <?php  } ?>

                                <?= $error['function'] ?>
                            <?php  } ?>
                            <?php if (isset($error['args'])) { ?>
                                <br /><span class="args">Args </span> : <pre> <?php print_r($error['args']); ?></pre>
                            <?php  } ?>
                            </div>
                    </article>
                <?php } ?>
            </section>
            
            <section id="originalText">
                <div class="title-bar">
                    <h2>Original text :</h2>
                </div>
                <article>
                    <div class="block-to-center">
                        <p>
                        <?php
                            foreach (explode('#', $error) as $error) {
                                echo $error . '<br/>';
                            }
                        ?>
                        </p>
                    </div>
                </article>
            </section>
        </main>
    </body>
</html>