<?php

/**
 * This file allow to catch any error and exception
 * ------------------------------------------------
 * 
 * @function set_exception_handler => Catch an Error or an Exception
 */

### Set the error/exception handler ###
set_exception_handler(function(Throwable $error) {
    function generateResponse(string $viewName, Throwable $error)
    {
        ### Affectation du coe d'erreur ###
        http_response_code($error->getCode());

        ### Génération de la vue à afficher à l'utilisateur ###
        # Chemin vers la vue d'erreur #
        $viewPath = __DIR__.'/views/'.$viewName.'.php';
        if(file_exists($viewPath)) {
            ob_start();
                include($viewPath);
            $view = ob_get_clean();
        } else {
            $view = 'La vue de déboggage '.$viewPath.' n\'existe pas.<br />'
            .'An error '.$error->getCode().' has occured : '.$error->getMessage().' at line '.$error->getLine().' in file '.$error->getFile().'.';
        }
        ### Affichage de la réponse ###
        echo $view;
    }
    
    if(defined('ENV')) {
        switch(ENV) {
            case 'prod':
                generateResponse('prod', $error);
            break;
            case 'dev':
            default:
                generateResponse('dev', $error);
            break;
        }
    } else {
        generateResponse('dev', $error);
    }
});
