<?php

namespace App\Core;

use Exception;

class View
{
    private $view;
    private $template;
    private $data = [];

    public function __construct($view, $template = 'back')
    {
        $this->setTemplate($template);
        $this->setView($view);
    }

    public function setTemplate($template)
    {
        $this->template = strtolower(trim($template));

        if (!file_exists('views/templates/' . $this->template . '.tpl.php')) {
            throw new Exception("Le template n'existe pas", 500);
        }
    }

    public function setView($view)
    {
        $this->view = strtolower(trim($view));

        if (!file_exists('views/' . $this->view . '.view.php')) {
            throw new Exception("La vue n'existe pas", 500);
        }
    }

    public function assign($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function addModal($modal, $data)
    {
        if (!file_exists('views/modals/' . $modal . '.mod.php')) {
            throw new Exception('Le modal ' . $modal . " n'existe pas", 500);
        }

        include 'views/modals/' . $modal . '.mod.php';
    }

    public function __destruct()
    {
        // $this->data = ["name"=>"yves"];
        //$name = "yves"
        extract($this->data);
        include 'views/templates/' . $this->template . '.tpl.php';
    }
}
