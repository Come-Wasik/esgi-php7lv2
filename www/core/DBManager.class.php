<?php

namespace App\Core;

use PDO;
use ReflectionClass;
use Exception;

class DBManager
{
    /**
     * @attribute $hdb = connexion PDO vers une base de données
     */
    private $dbh;

    /**
     * @function constructor = Créer une connexion à une base de données utilisable
     */
    public function __construct(BDDInterface $connexion)
    {
        if ($connexion !== null) {
            $this->dbh = $connexion;
        }
    }

    /**
     * @function save = Fusion de add et update
     */
    public function save(object $entity)
    {
        # Add
        if ($entity->getId() == null) {
            return $this->add($entity);
        # update
        } else {
            return $this->update($entity);
        }
    }

    /**
     * @function find = get an entity by an id
     */
    public function find(string $entityName, int $id): ?array
    {
        if (!class_exists($entityName)) {
            throw new Exception('Class ' . $entityName . ' doesn\'t exist', 500);
        }
        # On récupère le nom simple de la classe (sans le namespace)
        $tableName = $this->getOnlyClassname($entityName);

        $request = 'SELECT * FROM ' . $tableName . ' WHERE id = :id';

        $pdoSt = $this->dbh->prepare($request);
        if (!$pdoSt->execute(['id' => $id])) {
            $this->throwPdoError($request, $pdoSt->errorInfo());
        }
        /**
         * Si array YES
         * Si null Nop
         */

        $response = $pdoSt->fetchAll(PDO::FETCH_CLASS, $entityName);
        if (is_array($response) && !empty($response)) {
            return $response[array_key_first($response)];
        }
        return $response;
    }

    /**
     * @function findAll = Permet d'obtenir sous forme d'objet toutes les lignes d'un tableau
     */
    public function findAll(string $entityName)
    {
        if (!class_exists($entityName)) {
            throw new Exception('Class ' . $entityName . ' doesn\'t exist', 500);
        }
        # On récupère le nom simple de la classe (sans le namespace)
        $tableName = $this->getOnlyClassname($entityName);

        $request = 'SELECT * FROM ' . $tableName;

        $pdoSt = $this->dbh->prepare($request);
        if (!$pdoSt->execute([])) {
            $this->throwPdoError($request, $pdoSt->errorInfo());
        }

        return $pdoSt->fetchAll(PDO::FETCH_CLASS, $entityName);
    }

    /**
     * @function add = ajoute une entité dans la base de données
     */
    public function add(object $entity)
    {
        # On récupère le nom de la classe pour l'entité passé en argument
        $tableName = $this->getOnlyClassname(\get_class($entity));

        # On récupère les attributs et leur valeur contenus dans l'entité
        $varFromEntity = $this->getObjectAttr($entity);

        # On ne veut pas ajouter l'id dans la requête donc on fait le tri
        $columnData = $varFromEntity;
        unset($columnData['id']);
        $columnNames = array_keys($columnData);

        $request = 'INSERT INTO ' . $tableName . ' (' . implode(',', $columnNames) . ') VALUES (:' . implode(', :', $columnNames) . ')';

        $pdoSt = $this->dbh->prepare($request);
        if (!$pdoSt->execute($columnData)) {
            $this->throwPdoError($request, $pdoSt->errorInfo());
        }

        return 'Demande d\'ajout réussi avec succès. ' . $pdoSt->rowCount() . ' lignes ont été ajoutés.';
    }

    /**
     * @function update = Met à jour une entité dans la base de données
     */
    public function update(object $entity)
    {
        # On récupère le nom de la classe pour l'entité passé en argument
        $tableName = lcfirst($this->getOnlyClassname(\get_class($entity)));

        # On récupère les attributs et leur valeur contenus dans l'entité
        $varFromEntity = $this->getObjectAttr($entity);

        # On ne veut pas ajouter l'id dans la liste des données à mettre à jour donc on fait le tri
        $columnData = $varFromEntity;
        unset($columnData['id']);
        $columnNames = array_keys($columnData);

        $request = 'UPDATE ' . $tableName . ' SET '
        . implode(',', array_map(function ($attrName) {
            return $attrName . ' = :' . $attrName;
        }, $columnNames))
        . ' WHERE id = :id';

        $pdoSt = $this->dbh->prepare($request);
        if (!$pdoSt->execute($varFromEntity)) {
            $this->throwPdoError($request, $pdoSt->errorInfo());
        }

        return 'Demande de mise à jour réussi avec succès. ' . $pdoSt->rowCount() . ' lignes ont été ajoutés.';
    }

    /**
     * @function count = Retourne le nombre d'occurences d'une entité
     */
    public function count(string $entityName)
    {
        if (!class_exists($entityName)) {
            throw new Exception('Class ' . $entityName . ' doesn\'t exist', 500);
        }
        # On récupère le nom simple de la classe (sans le namespace)
        $tableName = $this->getOnlyClassname($entityName);

        $request = 'SELECT COUNT(*) as count FROM ' . $tableName;

        $pdoSt = $this->dbh->prepare($request);
        if (!$pdoSt->execute([])) {
            $this->throwPdoError($request, $pdoSt->errorInfo());
        }

        $count = $pdoSt->fetchColumn;
        return $count;
    }

    /**
     * Errors methods for Database Manager
     */
    private function throwPdoError(string $request, array $errors)
    {
        $errors[0] = 'SQL error code : ' . $errors[0];
        $errors[1] = 'Driver Error code : ' . $errors[1];
        $errors[2] = 'Error message : ' . $errors[2];
        throw new Exception('La requête ' . $request . ' n\'a pas pu aboutir. Raison : ' . implode('; ', $errors), 500);
    }

    /**
     * Appendix methods for Database Manager
     *
     * @function getOnlyClassname = Delete namespace from fully qualified class name to return the result
     * @function getObjectAttrName = Renvoi la liste des arguments d'un objet (seulement les noms)
     * @function getObjectAttr = Renvoi la liste des arguments d'un objet sous forme clé/valeur => nom/valeur
     */

    // Thanks to codeFareith : https://coderwall.com/p/cpxxxw/php-get-class-name-without-namespace
    private function getOnlyClassname(string $className): string
    {
        return substr($className, strrpos($className, '\\') + 1);
    }

    private function getObjectAttr(object $object)
    {
        $attributeList = [];
        foreach ($this->getObjectAttrName($object) as $attributeName) {
            $getter = 'get' . ucfirst($attributeName);
            $attributeList[$attributeName] = $object->$getter();
        }
        return $attributeList;
    }

    private function getObjectAttrName(object $object)
    {
        $ref = new ReflectionClass($object);
        $props = $ref->getProperties();
        return array_map(function ($refAttr) {
            return $refAttr->getName();
        }, $props);
    }
}
