<?php

namespace App\Core;

use PDO;
use Exception;

class PdoConnexion implements BDDInterface
{
    private $dbh;

    public function connexion(
        string $dbDriver,
        string $dbAddress,
        string $dbName,
        string $dbUser,
        string $dbPass
    ) {
        $this->dbh = new PDO($dbDriver . ':dbname=' . $dbName . ';host=' . $dbAddress, $dbUser, $dbPass);
    }

    public function query(string $query, array $parameters = [], string $fetchStyle = 'assoc', array $fetchOptions = [])
    {
        // $this->dhb =
        $pdoSt = $this->dbh->prepare($query);
        if (!$pdoSt->execute($parameters)) {
            $this->throwPdoError($query, $pdoSt->errorInfo());
        }

        # Initialisation d'un potentiel argument à envoyer à fetch. Peut changer selon sa méthode de filtre.
        $fetchArg = [];

        # Filtrage de la méthode fetch à employer
        switch ($fetchStyle) {
            case 'unique':
            case PDO::FETCH_UNIQUE:
                $fetchStyle = PDO::FETCH_UNIQUE;
                break;

            case 'group':
            case PDO::FETCH_GROUP:
                $fetchStyle = PDO::FETCH_GROUP;
                break;

            case 'both':
            case PDO::FETCH_BOTH:
                $fetchStyle = PDO::FETCH_BOTH;
                break;

            case 'column':
            case PDO::FETCH_COLUMN:
                /** @example :
                 *  $column = $manager->query('SELECT * FROM Epee', [], 'column', [
                 *      'column' => 3
                 *  ]);
                 */
                $fetchStyle = PDO::FETCH_COLUMN;
                $fetchArg[] = $fetchOptions['column'];
                break;

            case 'func':
            case PDO::FETCH_FUNC:
                /** @example :
                 *  $columns = $manager->query('SELECT name, power FROM Epee', [], 'func', ['function' => function($name, $power) {
                 *      return 'Nous avons une '.$name.' avec '.$power.' de puissance !';
                 *  }]);
                 */
                $fetchStyle = PDO::FETCH_FUNC;
                $fetchArg[] = $fetchOptions['function'];
                break;

            case 'class':
            case PDO::FETCH_CLASS:
                /** @example :
                 *  $object = $manager->query('SELECT * FROM Epee', [], 'class', [
                 *      'classname' => \App\Entity\Epee::class
                 *  ]);
                 */

                $fetchStyle = PDO::FETCH_CLASS;
                $fetchArg[] = $fetchOptions['classname'];

                $fetchArg[] = ($fetchOptions['constructorArgs'] ?? null);
                break;

            case 'assoc':
            case PDO::FETCH_ASSOC:
            default:
                $fetchStyle = PDO::FETCH_ASSOC;
                break;
        }

        return $pdoSt->fetchAll($fetchStyle, ...$fetchArg);
    }

    /**
     * Errors methods for Database Manager
     */
    private function throwPdoError(string $request, array $errors)
    {
        $errors[0] = 'SQL error code : ' . $errors[0];
        $errors[1] = 'Driver Error code : ' . $errors[1];
        $errors[2] = 'Error message : ' . $errors[2];
        throw new Exception('La requête ' . $request . ' n\'a pas pu aboutir. Raison : ' . implode('; ', $errors), 500);
    }
}
