<?php

use App\Core\ConstLoader;

### Importation de l'autoloader ###
spl_autoload_register(function ($className) {
    ### Extract namespace and classe name ###
    $namespaceParts = explode('\\', $className);
    $justClassName = array_pop($namespaceParts);
    $justNamespace = implode('\\', $namespaceParts);

    if (!empty($justNamespace)) {
        switch ($justNamespace) {
            case 'App\Controller':
                include __DIR__ . '/controllers/' . $justClassName . '.class.php';
                break;
            case 'App\Core':
                include __DIR__ . '/core/' . $justClassName . '.class.php';
                break;
            case 'App\Core\Manager':
                include __DIR__ . '/core/managers/' . $justClassName . '.class.php';
                break;
            case 'App\Model':
                include __DIR__ . '/models/' . $justClassName . '.model.php';
                break;
        }
    }
});

### Importation du gestionnaire d'erreurs ###
require __DIR__ . '/core/errormanaging/errorManaging.php';

### Chargement des constantes d'environnement ###
new ConstLoader();

### Gestion du routing ###
$uri = $_SERVER['REQUEST_URI'];
$listOfRoutes = yaml_parse_file('routes.yml');

if (!empty($listOfRoutes[$uri])) {
    $c = $listOfRoutes[$uri]['controller'] . 'Controller';
    $a = $listOfRoutes[$uri]['action'] . 'Action';

    //Est ce que dans le dossier controller il y a une class
    //qui correspond à $c
    if (class_exists($c)) {
        $controller = new $c();
        if (method_exists($controller, $a)) {
            $controller->$a();
        } else {
            throw new Exception("L'action' n'existe pas", 500);
        }
    } else {
        throw new Exception("Le class controller n'existe pas", 500);
    }
} else {
    throw new Exception("L'url n'existe pas : Erreur 404", 404);
}
