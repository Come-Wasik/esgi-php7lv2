<?php

namespace App\Model;

class BaseModel
{
    public function __construct()
    {
    }

    public function hydrate(array $data)
    {
        if ($data) { // Si $data n'est pas vide
            foreach ($data as $attName => $attValue) {
                $settername = 'set' . ucfirst($attName);
                $this->$settername($attValue);
            }
        }
    }
}
